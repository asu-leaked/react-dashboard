import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <p>
            Edit <code>src/App.js</code> and save to reload.
          </p> */}
          <a
            className="atom-link"
            href="/dashboard"
            target="_blank"
            rel="noopener noreferrer"
          ><img src={logo} className="atom" alt="logo" href="https://google.com"/></a>
        </header>
      </div>
    );
  }
}

export default App;
